#include "files.hh"

extern "C" {
  Files * init_Files(){
    return new Files();
  }
  Files * init_Files_filename(std::string file){
    return new Files(file);
  }
  Files * init_Files_filename_mode(std::string file,std::string mode){
    return new Files(file,mode);
  }
  void kill_em_all(Files* file_object){
    delete file_object;
  }
 
}

Files::Files(){
  openingMode="in";
}

// Ouverture en lecture 
Files::Files(std::string file){
  openingMode="in";
  setFileName(file);
  //create_File();
}
Files::Files(std::string file,std::string mode){
  //fileName=file;
  openingMode=mode;
  setFileName(file);
  //create_File();
}
Files::~Files(){
	closeFile();
}
void Files::setFileName(std::string file){
  fileName=file;
  create_File();
}


void Files::create_File(){
  if(openingMode=="out"){
    curentFileO=new std::ofstream(fileName.c_str(),std::fstream::out);  
  }
  else{
    curentFile=new std::ifstream(fileName.c_str(),std::fstream::in);
  }
}
int Files::openFile(){
  if(openingMode=="out"){
    if(curentFileO->is_open()){
      return 1;
    }
    else return 0;
  }
  else{
    if(curentFile->is_open()){
      return 1;
    }
    else return 0;
  }
}
int Files::closeFile(){
  if(openingMode=="out")
    curentFileO->close();
  else
    curentFile->close();
 return 0;
}
std::string Files::readFile(){
  *curentFile >> ResRead;
  return ResRead;
}
int Files::writeFile(std::string message){
  *curentFileO << message;
  return -1;
}
std::string Files::readLine(){
  std::string line="";
  getline (*curentFile,line);
  return line;
}
