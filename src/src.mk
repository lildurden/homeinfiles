SRC=$(wildcard $(ROOT)/$(DIR_SRC)/*.cpp)
OBJ=$(SRC:$(ROOT)/$(DIR_SRC)/%.cpp=$(ROOT)/$(DIR_OBJ)/%.o)

# Compilation of the program
$(EXEC): $(OBJ) #$(DYNAMIC_LIBS:%=$(ROOT)/$(DIR_LIB)/%.so)
	@echo "Linking ..... : $@"
	@$(GXX) -o $@ $^ $(CFLAGS)

# Compilation from sources (.cpp) to objects (.o)
$(ROOT)/$(DIR_OBJ)/%.o: $(ROOT)/$(DIR_SRC)/%.cpp
	@echo "Compiling ... : $@"
	@$(GXX) -c -o $@ $< $(CFLAGS_OBJ)
