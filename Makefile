include ./standard_def.mk

EXEC=$(ROOT)/$(DIR_BIN)/HomeInFile

LIBR=$(ROOT)/$(DIR_LIB)/HomeInFile.so

all:: createdirectories $(EXEC)

createdirectories::
	@mkdir -p $(DIR_OBJ) $(DIR_LIB) $(DIR_BIN)


-include $(ROOT)/$(DIR_SRC)/src.mk


.PHONY:: clean doc lib

lib:	createdirectories $(LIBR)

-include $(ROOT)/$(DIR_SRC)/libsrc.mk

doc:
	@echo -n "Generation de la DOC ... "
	@mkdir -p $(DIR_DOC)
	@doxygen 
	@echo "OK."

clean::
	@echo "Cleaning Project"
	@rm -rf $(DIR_OBJ) $(DIR_LIB) $(DIR_BIN) $(DIR_DOC)

raz:: 
	@echo "Cleaning Project++"
	@rm -rf $(DIR_OBJ) $(DIR_LIB) $(DIR_BIN) $(DIR_DOC) $(DIR_PLO) $(DIR_OUT)
